`import Ember from 'ember'`

IndexController = Ember.ArrayController.extend

  name: null
  message: null

  actions:

    createMessage: ->
      @store.createRecord('user', {
        name: @get('name')
        time: new Date()
        message: @get('message')
      }).save()
      @setProperties
        message: null

`export default IndexController`
