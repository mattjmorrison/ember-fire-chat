`import DS from 'ember-data'`

User = DS.Model.extend
  name: DS.attr('string'),
  message: DS.attr('string')
  time: DS.attr('date')

  formattedMessage:(->
    hours = @get('time').getHours()
    minutes = @get('time').getMinutes()
    "[#{hours}:#{minutes}] #{@get('name')}: #{@get('message')}"
  ).property('time', 'message', 'name')


`export default User`
